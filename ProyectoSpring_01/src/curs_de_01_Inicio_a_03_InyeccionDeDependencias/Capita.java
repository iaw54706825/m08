package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Capita implements Tripulants {
	private InformeInterface informeNou;
	
	
	public Capita(InformeInterface informeNou) {
		super();
		this.informeNou = informeNou;
	}
	public String agafarTarees() {
		return "Soc el capita e la nau Leonov.";
	}
	
	@Override
	public String agafarInforme() {
		return "Informe del Capita. " + informeNou.getInforme();
	}
}
