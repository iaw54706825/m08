package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Electronic implements Tripulants{
	private InformeInterface InformeNou;
	private String email;
	private String nomDepartamente;		
	
	public void setInformeNou(InformeInterface informeNou) {
		this.InformeNou = informeNou;
	}
		
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public String getNomDepartamente() {
		return nomDepartamente;
	}

	public void setNomDepartamente(String nomDepartamente) {
		this.nomDepartamente = nomDepartamente;
	}



	@Override
	public String agafarTarees() {
		return "Aquesta son les tarees del Electronic";
	}
	
	@Override
	public String agafarInforme() {
		return "Informe del Electronic. " + InformeNou.getInforme();
	}
}
