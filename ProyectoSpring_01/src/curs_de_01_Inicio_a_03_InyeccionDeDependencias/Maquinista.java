package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Maquinista implements Tripulants{
	private InformeInterface InformeNou;

	public void setInformeNou(InformeInterface informeNou) {
		this.InformeNou = informeNou;
	}
	private String email;
	private String nomDepartament;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomDepartament() {
		return nomDepartament;
	}
	public void setNomDepartament(String normDepartament) {
		this.nomDepartament = normDepartament;
	}
	
	@Override
	public String agafarTarees() {
	return "Aquesta son les tarees del Maquinista";
	}
	
	@Override
	public String agafarInforme() {
		return "Informe del Maquinista" + InformeNou.getInforme();
	}
}
