package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

public class Navegant implements Tripulants {
	private InformeInterface informeNou;
	
	public void setInformeNou(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}

	@Override
	public String agafarTarees() {
		return "agafarTarees(): Soc el navegant de la nau Leanov.";
	}

	@Override
	public String agafarInforme() {
		return "agafarInforme(): Informe del navegant. " + informeNou.getInforme();
	}
	
	public void metodeInit() {
		System.out.println("Acabem de llançar el metode init() de la classe Navegant.class just abans de que el bean estigui llest.");		
	}
	
	public void metodeDestroy() {
		System.out.println("Acabem de llançar el metode destroy() de la classe Navegant.class just despres de que el bean hagi estat destruit.");	

	}
}
