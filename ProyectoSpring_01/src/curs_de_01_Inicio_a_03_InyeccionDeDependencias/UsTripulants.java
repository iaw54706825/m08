package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsTripulants {

	public static void main(String[] args) {
		/*Tripulants capita = new Capita();
		Tripulants maquinista = new Maquinista();
		Tripulants electronic = new Electronic();
		
		System.out.println(capita.agafarTarees());
		System.out.println(maquinista.agafarTarees());
		System.out.println(electronic.agafarTarees());		
		*/
		//Creamos el contexto cargando el fichero xml de configuacion.
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Creamos objeto haciendo servir el bean
		
		Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees());
		System.out.println(capita.agafarInforme()+"\n");
		
		
		
		Tripulants maquinista = contexte.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees()+"\n");
		
		Maquinista maquinista_2 = contexte.getBean("tripulantMaquinista", Maquinista.class);
		System.out.println(maquinista_2.getEmail());
		System.out.println(maquinista_2.getNomDepartament());
		
		
		
		Tripulants electronic = contexte.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees());
		System.out.println(electronic.agafarInforme() +"\n");
		
		Electronic electronic_2 = contexte.getBean("tripulantElectronic", Electronic.class);
		System.out.println("Email: " + electronic_2.getEmail());
		System.out.println("Departament: " + electronic_2.getNomDepartamente());
		
		contexte.close();	
				
	}
}
