package curs_de_01_Inicio_a_03_InyeccionDeDependencias;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usInitDestroy {

	public static void main(String[] args) {
		System.out.println("ABANS DE CARREGAR EL CONTEXT.");
		
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_InitDestroy.xml");
		
		System.out.println("DESPRES DE CARREGAR EL CONTEXT I ABANS DE CARREGAR EL BEAN");
		
		
		//Preticio de beans al contenidor
		Navegant navegant = contexte.getBean("tripulantNavegant", Navegant.class);
		
		System.out.println("DESPRES DE CARREGAR EL BEAN");		
		System.out.println(navegant.agafarInforme());
		System.out.println("DESPRES D'EXECUTAR navegant.agafarInforme()");
		System.out.println(navegant.agafarTarees());
		System.out.println("DESPRES D'EXECUTAR navegant.agafarTarees()");
		
		contexte.close();
		
		System.out.println("DESPRES DE TANCAR EL CONTEXT.");

	}

}
