package exercici;

import java.time.LocalDate;
import java.util.ArrayList;

public class Ruta {
	private int id;
	private String nom;
	private Waypoint waypoint;
	private ArrayList<Waypoint> llistaWaypointsArrayList;
	private LocalDate dataCreacioDate;

	/**
	 * @param waypoint
	 */
	public Ruta(Waypoint waypoint) {
		this.waypoint = waypoint;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the waypoint
	 */
	public Waypoint getWaypoint() {
		return waypoint;
	}

	/**
	 * @param waypoint the waypoint to set
	 */
	public void setWaypoint(Waypoint waypoint) {
		this.waypoint = waypoint;
	}

	/**
	 * @return the llistaWaypointsArrayList
	 */
	public ArrayList<Waypoint> getLlistaWaypointsArrayList() {
		return llistaWaypointsArrayList;
	}

	/**
	 * @param llistaWaypointsArrayList the llistaWaypointsArrayList to set
	 */
	public void setLlistaWaypointsArrayList(ArrayList<Waypoint> llistaWaypointsArrayList) {
		this.llistaWaypointsArrayList = llistaWaypointsArrayList;
	}

	/**
	 * @return the dataCreacioDate
	 */
	public LocalDate getDataCreacioDate() {
		return dataCreacioDate;
	}

	/**
	 * @param dataCreacioDate the dataCreacioDate to set
	 */
	public void setDataCreacioDate(LocalDate dataCreacioDate) {
		this.dataCreacioDate = dataCreacioDate;
	}

}
