package exercici;

import java.time.LocalDate;

public class Waypoint {
	private int id;
	private String nom;
	private LocalDate dataCreacioDate;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the dataCreacioDate
	 */
	public LocalDate getDataCreacioDate() {
		return dataCreacioDate;
	}
	/**
	 * @param dataCreacioDate the dataCreacioDate to set
	 */
	public void setDataCreacioDate(LocalDate dataCreacioDate) {
		this.dataCreacioDate = dataCreacioDate;
	}
	
	@Override
	public String toString() {
		return "Waypoint [id=" + id + ", nom=" + nom + ", dataCreacioDate=" + dataCreacioDate + "]";
	}		
	
	

}
