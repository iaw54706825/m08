/**
 * 
 */
package exercici;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author iaw54706825
 *
 */
public class usExercici {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Exercici.xml");

		System.out.println("------------Exercici 1------------");
		//Peticion de beans al contenedor Spring.
		Ruta rutaSingleton_1 = contexte.getBean("rutaSingleton", Ruta.class);
		Ruta rutaSingleton_2 = contexte.getBean("rutaSingleton", Ruta.class);
		
		System.out.println("Direccio memoria rutaSingleton_1: " + rutaSingleton_1);
		System.out.println("Direccio memoria rutaSingleton_1: " + rutaSingleton_2);
		System.out.println("-------");
		System.out.println("rutaSingleton_1.getId(): " + rutaSingleton_1.getId());
		System.out.println("rutaSingleton_2.getNom(): " + rutaSingleton_1.getNom());
		System.out.println("rutaSingleton_2.getWaypoint()(): " + rutaSingleton_1.getWaypoint());
		System.out.println("rutaSingleton_2.getLlistaWaypoints(): " + rutaSingleton_1.getLlistaWaypointsArrayList());
		System.out.println("rutaSingleton_2.getWaypoint().getId(): " + rutaSingleton_1.getWaypoint().getId());
		System.out.println("rutaSingleton_2.getWaypoint().getNom(): " + rutaSingleton_1.getWaypoint().getNom());
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("------------Exercici 2------------");
		Ruta rutaPrototype_1 = contexte.getBean("rutaPrototype", Ruta.class);
		Ruta rutaPrototype_2 = contexte.getBean("rutaPrototype", Ruta.class);
		
		System.out.println("Direccio memoria rutaPrototype_1: " + rutaPrototype_1);
		System.out.println("Direccio memoria rutaPrototype_2: " + rutaPrototype_2);
		System.out.println("-------");
		System.out.println("rutaPrototype_1.getId(): " + rutaPrototype_1.getId());
		System.out.println("rutaPrototype_1.getNom(): " + rutaPrototype_1.getNom());
		System.out.println("rutaPrototype_1.getWaypoint()(): " + rutaPrototype_1.getWaypoint());
		System.out.println("rutaPrototype_1.getLlistaWaypoints(): " + rutaPrototype_1.getLlistaWaypointsArrayList());
		System.out.println("rutaPrototype_1.getWaypoint().getId(): " + rutaPrototype_1.getWaypoint().getId());
		System.out.println("rutaPrototype_1.getWaypoint().getNom(): " + rutaPrototype_1.getWaypoint().getNom());
		System.out.println("-------");
		System.out.println("rutaPrototype_2.getId(): " + rutaPrototype_2.getId());
		System.out.println("rutaPrototype_2.getNom(): " + rutaPrototype_2.getNom());
		System.out.println("rutaPrototype_2.getWaypoint()(): " + rutaPrototype_2.getWaypoint());
		System.out.println("rutaPrototype_2.getLlistaWaypoints(): " + rutaPrototype_2.getLlistaWaypointsArrayList());
		System.out.println("rutaPrototype_2.getWaypoint().getId(): " + rutaPrototype_2.getWaypoint().getId());
		System.out.println("rutaPrototype_2.getWaypoint().getNom(): " + rutaPrototype_2.getWaypoint().getNom());
		
		
		System.out.println();
		System.out.println("L' objecte waypoint de rutaPrototype_1 i rutaPrototype_2 es el mateix perque te la"
				+ "mateixa dir. de memoria ja que el beans que crea els objectes de tipus Waypoint es Singleton");
		System.out.println("Aixo implica que 2 rutes diferents (rutaPrototype_1 i rutaPrototype_2) tenem com"
				+ "atribut el mateix objecte de la classe Waypoint (el comparteixen).");

		contexte.close();

		
	}

}
