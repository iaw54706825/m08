package curs_de_04_JavaAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component
public class ControladorSondes implements Tripulants {
	
	//-----METODOS INICIALIZADOR y FINALIZADOR------
	public ControladorSondes() {
		
	}
	
	//INICIALIZADOR
	@PostConstruct 
	public void inicialitzador() {
		System.out.println("ControladorSondes: S'HA EXECUTAR inicialitzador() AMB LA JAVA ANNOTATION @PostConstruct.");
	}
	
	@PreDestroy
	public void finalitzador() {
		System.out.println("ControladorSondes: S'HA EXECUTAR finalitzador() AMB LA JAVA ANNOTATION @PreDestroy.");
	}
	
	

	@Override
	public String agafarTarees() {
		return "controladorSondesNau: agafarTarees()";
	}

	@Override
	public String agafarInforme() {
		return "controladorSondesNau: agafarInforme()";
	}

}
