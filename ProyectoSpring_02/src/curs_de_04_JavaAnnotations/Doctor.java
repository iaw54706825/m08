package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("doctorNau")
public class Doctor implements Tripulants {
	private InformeInfermeriaInterface informeDoctor;
	
	
	/**
	 * @param informeDoctor
	 */
	@Autowired
	//Si solo hay 1 constructor en la clase, no es necesario utilizar el @Autowrired
	public Doctor(InformeInfermeriaInterface informeDoctor) {
		this.informeDoctor = informeDoctor; 
	}

	@Override
	public String agafarTarees() {
		return "doctorNau: agafarTarees(): curar als tripulants.";
	}

	@Override
	public String agafarInforme() {
		return "doctorNau: agafarInfome(): Informe del doctor de la missio." + informeDoctor.getInformeInfermeria();
	}

}
