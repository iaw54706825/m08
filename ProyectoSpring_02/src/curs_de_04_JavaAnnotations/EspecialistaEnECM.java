package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class EspecialistaEnECM implements Tripulants {	
	//Atributos
	private InformeElectronicaInterface informeEspecialistaEnECM;
	private InformeElectronicaInterface informeEspecialistaEnESM;
	private InformeElectronicaInterface informeEspecialistaEnEPM;
	@Value("${emailDepartamentECM}")
	private String email;
	@Value("${departamentECMNom}")
	private String departamentNom;
	
	//--CONSTRUCTORES
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}	
	
	/**
	 * @param informeEspecialistaEnECM
	 * @param informeEspecialistaEnESM
	 * @param informeEspecialistaEnEPM
	 */
	@Autowired
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM,
			InformeElectronicaInterface informeEspecialistaEnESM,
			InformeElectronicaInterface informeEspecialistaEnEPM) {
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
		this.informeEspecialistaEnESM = informeEspecialistaEnESM;
		this.informeEspecialistaEnEPM = informeEspecialistaEnEPM;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the departamentNom
	 */
	public String getDepartamentNom() {
		return departamentNom;
	}


	@Override
	public String agafarTarees() {
		return "EspecialistaEnECM: agafarTarees(): arreglar la electronica d'Infemeria";
	}

	@Override
	public String agafarInforme() {
		return informeEspecialistaEnECM.getInformeElectronica();
	}
	
//	@Override
//	public String ferManteniment() {
//		return "EspecialistaEnECM: ferManteniment();
//	}
	
	public String imprimirTripleInforme() {
		String cadena = "EspecialistaEnECM: imprimirTripleInforme(): \n \t" +
				"Informe ECM: " + informeEspecialistaEnECM.getInformeElectronica() + "\n\t" +
				"Informe ESM: " + informeEspecialistaEnESM.getInformeElectronica() + "\n\t" +
				"Informe EPM: " + informeEspecialistaEnEPM.getInformeElectronica();
		return cadena;
	}
}
