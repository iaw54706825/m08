package curs_de_04_JavaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("infermeraNau")

@Scope("prototype")
public class Infermera implements Tripulants {
	private InformeInfermeriaInterface informeInfermera;
	@Autowired
	@Qualifier("mantenimentInfermeriaMaterial")
	private MantenimentInfermeriaInterface mantenimentMaterial;
	@Autowired
	@Qualifier("mantInferMedica")
	private MantenimentInfermeriaInterface mantenimentMedicaments;

	
	//Autoried con Setter
	@Autowired
	public void setInformeInfermera(InformeDoctor informeInfermera) {
		this.informeInfermera = informeInfermera;
	}

	@Override
	public String agafarTarees() {
		// TODO Auto-generated method stub
		return "infermeraNau: agafarTarees(): curar als tripulants.";

	}

	@Override
	public String agafarInforme() {
		// TODO Auto-generated method stub
		return "infermeraNau: agafarInfome(): Informe de la infermera de la missio." + informeInfermera.getInformeInfermeria();
	}
	
	//////////
	public void mantenimientos() {
		System.out.println(mantenimentMaterial.ferMantenimentInfermeria());
		System.out.println(mantenimentMedicaments.ferMantenimentInfermeria());
	}
}
