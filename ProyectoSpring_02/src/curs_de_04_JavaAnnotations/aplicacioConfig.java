package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration//Sustituyre el fichero XML
@ComponentScan("curs_de_04_JavaAnnotations")//Paquete para buscar las clases para Spring
@PropertySource("classpath:dadesNau.properties")
public class aplicacioConfig {
	//Clase que sustituye el fichero xml de configuracion
	
	//Bean para crear el objeto InformeECM()
	@Bean
	public InformeElectronicaInterface informeECM() {
		return new InformeECM();
	}
	
	//Bean para crear el objeto InformeECM()
	@Bean
	public InformeElectronicaInterface informeESM() {
		return new InformeESM();
	}	
	
	//Bean para crear el objeto InformeECM()
	@Bean
	public InformeElectronicaInterface informeEPM() {
		return new InformeEPM();
	}	
	
	//Bean para crear el objeto especialistaEnECM()
	//Este Buean tiene una dependencia de la clase informeECM(), la inyectamos
	@Bean
	public Tripulants especialistaEnECM() {
		return new EspecialistaEnECM(informeECM());
	}
	
	//Bean para crear un objeto con @Autowrired
	@Bean
	public Tripulants especialistaEnECMTripleInforme() {
		return new EspecialistaEnECM(informeECM(), informeESM(), informeEPM());
	}
}
