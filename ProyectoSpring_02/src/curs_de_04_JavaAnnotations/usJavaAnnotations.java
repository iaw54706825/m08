package curs_de_04_JavaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotations {

	public static void main(String[] args) {
		System.out.println("---------usJavaAnnotations - INICI ---------");
		System.out.println();
		
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//@Component & @Autowired
		
		Tripulants doctor = contexte.getBean("doctorNau", Doctor.class);
		Infermera infermera = contexte.getBean("infermeraNau", Infermera.class);
		
		//Doctor @Component
		System.out.println("Crear objetos con @Component");
		System.out.println(doctor.agafarTarees());
		System.out.println(doctor.agafarInforme());
		System.out.println("----------------\n\n");
		
		//Infermera @Component & @Autowired
		System.out.println("Inyeccion de dependencias con @Autowired:");
		System.out.println(infermera.agafarTarees());
		System.out.println(infermera.agafarInforme());
		System.out.println("----------------\n\n");
		
		//Infermera @Component & @Autowired & @Qualifier
		System.out.println("Inyeccion de dependencias con @Autowired & @Qualifier");
		infermera.mantenimientos();
		System.out.println("\n---------usJavaAnnotations - FI ---------");

		
		contexte.close();		
	}
}
