package curs_de_04_JavaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotationsApartirDeScope {

	public static void main(String[] args) {
		System.out.println("------ usJavaAnnotationsApartirDeScope - INICI-------");
		System.out.println();
		
		//Creamos contexto CON ARCHIVO XML!!!
		//ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		//SCOPE - INICI:
		System.out.println("-------Annotations SCOPE-------");

		//DOCTOR
		Tripulants doctor_1 = contexte.getBean("doctorNau", Doctor.class);
		Tripulants doctor_2 = contexte.getBean("doctorNau", Doctor.class);
		System.out.println("Patro SINGLETON:");
		System.out.println("Dir. de memoria de doctor_1:" +  doctor_1);
		System.out.println("Dir. de memoria de doctor_2:" +  doctor_2);
		
		//ENFERMERA
		Tripulants infermera_1 = contexte.getBean("infermeraNau", Infermera.class);
		Tripulants infermera_2 = contexte.getBean("infermeraNau", Infermera.class);
		System.out.println("Patro PROTOTYPE:");
		System.out.println("Dir. de memoria de infermera_1:" +  infermera_1);
		System.out.println("Dir. de memoria de infermera_2:" +  infermera_2);
		
		//SCOPE - FI:		
		System.out.println();
		System.out.println();
	

		//------------------------------------------------------------
//		System.out.println("-------Annotations @PostConstruct & @PreDestroy-------");
//		System.out.println();
//		
//		//Creamos controlador De sondes
//		ControladorSondes controladorSondes = contexte.getBean("controladorSondes", ControladorSondes.class);
//		System.out.println("DESPRES DE CARREGAR EL BEAN.");
//		System.out.println(controladorSondes.agafarInforme());
//		System.out.println("DESPRES D'EXECUTAR controladorSondes.agafarInforme()");
//		System.out.println(controladorSondes.agafarTarees());
//		System.out.println("DESPRES D'EXECUTAR controladorSondes.agafarTarees()");
//
//		System.out.println("DESPRES DE TANCER EL CONTEXT");
		
		
		//Crear objetos con @Bean
		System.out.println();
		System.out.println("-----------@Bean Annotation------------");
		Tripulants especialistaEnECM_1 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		//Tripulants especialistaEnECM_1 = contexte.getBean("especialistaEnECM", Tripulants.class);  //Hace lo mismo
		System.out.println("Tripulants especialistaEnECM.agafarInforme():" + especialistaEnECM_1.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.agafarTarees():" + especialistaEnECM_1.agafarTarees());

		System.out.println();
		System.out.println("-----------Java Annotation @PropertySource------------");
		EspecialistaEnECM especialistaEnECM_2 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		System.out.println(especialistaEnECM_2.getDepartamentNom());
		System.out.println(especialistaEnECM_2.getEmail());
		
		
		System.out.println();
		System.out.println("-----------Ampliació de @Autowired en constructor i @Bean------------");
		
		Tripulants especialistaEnECM_10 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		System.out.println("Tripulants especialistaEnECM.agafarInforme()" +  especialistaEnECM_10.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.aagafarTarees()" +  especialistaEnECM_10.agafarTarees());

		EspecialistaEnECM especialistaEnECM_20 = contexte.getBean("especialistaEnECMTripleInforme", EspecialistaEnECM.class);
		System.out.println("Tripulants especialistaEnECMTripleInforme.imprimirTripleInforme()" + especialistaEnECM_20.imprimirTripleInforme() );
		System.out.println("Tripulants especialistaEnECMTripleInforme.aagafarTarees()" +  especialistaEnECM_20.agafarTarees());
		System.out.println("Tripulants especialistaEnECMTripleInforme.getEmail()" +  especialistaEnECM_20.getEmail());
		System.out.println("Tripulants especialistaEnECMTripleInforme.getDepartamentNom()" +  especialistaEnECM_20.getDepartamentNom());

		contexte.close();
		System.out.println("\n------ usJavaAnnotationsApartirDeScope - FI-------");

		
		

	}

}
