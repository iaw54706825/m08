package practica01_04JA;

import org.springframework.stereotype.Component;

@Component
public class Organitzacio {
	private String nom;
	private Informe informe;
	
	public Organitzacio(Informe informe) {
		this.informe = informe;
	}

	@Override
	public String toString() {
		return "Organitzacio [nom=" + nom + "]";
	}		
}
