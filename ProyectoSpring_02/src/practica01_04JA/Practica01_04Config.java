package practica01_04JA;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("practica01_04JA")
@PropertySource("classpath:practica01-04Departaments.properties")
public class Practica01_04Config {
	
	@Bean
	public Informe informe() {
		return new InformeOrganitzacio();
	}
	
	@Bean
	public Organitzacio organitzacio() {
		return new Organitzacio(informe());
	}

	@Bean
	public dep_B_per_2 personal_B() {
		return new dep_B_per_2();
	}
	
	@Bean
	public List<dep_B_per_2> llistaPersonal() {		
		return Arrays.asList(new dep_B_per_2(), new dep_B_per_2(), new dep_B_per_2());
	}
}
