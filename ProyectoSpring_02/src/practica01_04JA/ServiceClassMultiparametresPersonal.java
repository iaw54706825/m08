package practica01_04JA;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceClassMultiparametresPersonal {
	private final BeanFactory factory;
	private List<dep_B_per_2> llistaPersonalMultiparametres;
	
	@Autowired
	public ServiceClassMultiparametresPersonal(final BeanFactory f) {
		this.factory = f;
	}
	
	public void demoMethod(List<String> llistaDniPersonal, List<String> llistaNomPersonal) {
		// TODO Auto-generated method stub
		this.llistaPersonalMultiparametres = llistaDniPersonal.stream().map(param -> factory.getBean(dep_B_per_2.class, param))
				.collect(Collectors.toList());	
		
		int index = 0;
		for (dep_B_per_2 dept : this.llistaPersonalMultiparametres) {
			dept.setNom(llistaNomPersonal.get(index));			
			index++;
		}
		
	}

	public List<dep_B_per_2> getLlistaPersonalMultiparametres() {
		// TODO Auto-generated method stub
		return llistaPersonalMultiparametres;
	}

}
