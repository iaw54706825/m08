package practica01_04JA;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component
public class dep_B_per_1 implements PersonalInterface {
	public String dni;
	public String nom;
	public String cognom;
	public String email;
	public int numDepartament;
	
	@PreDestroy
	public void mensaje() {
		System.out.println("maas mataooo");
	}

	@Override
	public void saludo() {
		System.out.println("Pertenezco a dep_B_per_1");
	}	
}
