package practica01_04JA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class dep_B_per_2 implements PersonalInterface {
	public String dni;
	public String nom;
	public String cognom;
	public String email;
	public int numDepartament;	
	

	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @param numDepartament the numDepartament to set
	 */
	public void setNumDepartament(int numDepartament) {
		this.numDepartament = numDepartament;
	}


	/**
	 * @return the numDepartament
	 */
	public int getNumDepartament() {
		return numDepartament;
	}	


	
	@Override
	public void saludo() {
		System.out.println("Pertenezco a dep_B_per_2");
	}	
}
