package practica01_04JA;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class usAplicacion {
	/**
	 * Peronal
	 * @return
	 */
	public static List<dep_B_per_2> inicialitzarPersonal() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(Practica01_04Config.class);

		ServiceClassMultiparametresPersonal serviceClassMultiparametres = contexteAmbClasseConfig
				.getBean("serviceClassMultiparametresPersonal", ServiceClassMultiparametresPersonal.class);

		List<String> llistaDniPersonal = Arrays.asList("10", "20", "30");
		List<String> llistaNomPersonal = Arrays.asList("tripulant_10", "tripulant_20", "tripulant_30");
		serviceClassMultiparametres.demoMethod(llistaDniPersonal, llistaNomPersonal);
		contexteAmbClasseConfig.close();
		return serviceClassMultiparametres.getLlistaPersonalMultiparametres();
	}
}
