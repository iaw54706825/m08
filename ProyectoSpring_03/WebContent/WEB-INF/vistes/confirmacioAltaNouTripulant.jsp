<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmacio alta tripulant</title>
</head>
<body>
	S'ha donat d'alta el tripulant
	<b>${el_doctor.nom} ${el_doctor.cognom}</b> el qual te
	<b>${el_doctor.edat}</b> anys
	<br>Email del nou tripulant: <b>${el_doctor.email}</b>
	<br> El departament del nou tripulant es
	<b>${el_doctor.departament}</b>
	<br> Els coneixements del nou tripulant son
	<b>${el_doctor.coneixements}</b>
	<br> La ciutat de naixement del nou tripulant es
	<b>${el_doctor.ciutatNaixement}</b>
	<br> Els idiomes que sap del nou tripulant son
	<b>${el_doctor.idiomes}</b>
	<br>El codi postal del nou tripulant es <b>${el_doctor.codiPostal}</b>
</body>
</html>