package curs_de_05_AplicacionesWeb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	
	@RequestMapping // es lo mismo que : 	@RequestMapping("/")
	public String mostrarVista1() {
		return "vistaExemple_1";
	}

}
