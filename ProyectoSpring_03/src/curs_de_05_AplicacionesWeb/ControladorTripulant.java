package curs_de_05_AplicacionesWeb;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tripulant")
public class ControladorTripulant {

	@RequestMapping("/mostrarFormulariAltaTripulant")
	public String mostrarFormulari(Model model) {
		Tripulant doctor = new Tripulant();
		model.addAttribute("el_doctor", doctor);
		return "formulariAltaTripulant";
	}

	@RequestMapping("/procesarAltaTripulant")
	public String mostrarFormulari(@Valid @ModelAttribute("el_doctor") Tripulant elNouTripulant,
			BindingResult resultatValidacio) {

		if (resultatValidacio.hasErrors()) {
			return "formulariAltaTripulant";
		} else {
			return "confirmacioAltaNouTripulant";
		}
	}
	
	@InitBinder
	public void restallarEspaisEnblanc(WebDataBinder binder) {
		StringTrimmerEditor objRetalladorEspaisEnBlanc = new StringTrimmerEditor(true);
		
		binder.registerCustomEditor(String.class, objRetalladorEspaisEnBlanc);		
	}

}
