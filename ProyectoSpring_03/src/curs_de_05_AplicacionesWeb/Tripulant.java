package curs_de_05_AplicacionesWeb;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Tripulant {
	@NotNull
	@Size(min = 2, message = "Long. minima del nom ha de ser 2 caracters.")
	private String nom;
	private String cognom;
	@NotNull
	@Min(value = 18, message = "El nou tripulant ha de tenir com a minim 18 anys")
	@Max(value = 65, message = "El nou tripulant ha de tenir com a maxim 65 anys")
	private int edat;
	private String departament;
	private String coneixements;
	private String ciutatNaixement;
	private String idiomes;
	@NotBlank(message = "S' ha d' omplir l'email.")
	@Size(min = 5, message = "Email no valid.")
	@Email
	private String email;
	@Pattern(regexp="[0-9]{5}", message = "Nomes 5 numeros")
	private String codiPostal;
	
	// Getters & Setters
	
	public String getEmail() {
		return email;
	}
	public String getCodiPostal() {
		return codiPostal;
	}
	public void setCodiPostal(String codiPostal) {
		this.codiPostal = codiPostal;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}
	public String getIdiomes() {
		return idiomes;
	}
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	public String getConeixements() {
		return coneixements;
	}
	public void setConeixements(String coneixements) {
		this.coneixements = coneixements;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom() {
		return cognom;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}

	
}
