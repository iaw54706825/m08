<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>formulariNomResposta.jsp</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/elMeuEstil.css">
</head>
<body>
	Video 28 (omplert el formulari del video 28): <br>
	Aquesta es la resposta que envia el server al formulari que ha enviat l'usuari.<br>
	El nom del tripulant enviat per l'usuari a traves del formulari ha estas: <b> ${param.tripulantNom_28}</b>
	<br>
	<br>
	Video 29 (omplert el formulari del video 29):<br>
	<b>missatgePerRetornar: </b> ${missatgePerRetornar_29}	
	<br>
	<img alt="imatge d'un poio" width=50% src="${pageContext.request.contextPath}/recursos/imatges/img1.jpg">
</body>
</html>