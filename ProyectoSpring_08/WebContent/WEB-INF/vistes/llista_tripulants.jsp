<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LLista de tripulants</title>
</head>
<body>
	<h1>LLista de tripulants</h1>
	<table>
		<tr>
			<th> ID </th>
			<th> Nom </th>
			<th> Cognom </th>
			<th> Email </th>
			<th> Data alta </th>
			<th> Departament ID </th>
		</tr>
		<c:forEach var="tripulantTmp" items="${llistaTripulants}">
			<tr>
				<td>${tripulantTmp.id}</td>
				<td>${tripulantTmp.nom}</td>
				<td>${tripulantTmp.cognom}</td>
				<td>${tripulantTmp.email}</td>
				<td>${tripulantTmp.dataCreacio}</td>
				<td>${tripulantTmp.departamentId}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>