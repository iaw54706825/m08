package connexioBD;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ConnexioAmbBD
 */
@WebServlet("/ConnexioAmbBD")
public class ConnexioAmbBD extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnexioAmbBD() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		// Hem d'especificar la URL (la direcció) on es trova el nostre servidor MySQL (el de la BD).
		// El format per a connectar-se a una BD MySQL és: "jdbc:mysql://" + la IP del servidor + ":" + el port de sortida
		// del server MySQL (per defecte és el 3306) + "/" + nom de la nostra BD.
		// Li afegim "useSSL=false" per a indicar que ens connectarem amb la BD sense seguretat SSL.
		String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD"; 
		
		// Si hi hagués cap error referent a la "time zone", hem d'afegir &serverTimezone=UTC al final de servidorBDUrl
		// perquè al final ens quedi: 
		// String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD?useSSL=false&serverTimezone=UTC";
		
		// Indiquem quin és l'usuari i el password que farem servir per a connectar-nos a la BD del MySQL.
		
		String usuari = "root";
		
//		String contrasenya = "88pak75gpr";
		String contrasenya = "";

		
		// Aquest és el nom que té la versió 8 del driver mysql (el driver és el fitxer 'mysql-connector-java-8.0.19.jar' 
		// que hem ficat en la carpeta WebContent/WEB-INF/lib).
		String driver = "com.mysql.cj.jdbc.Driver";

		try {

			// Per a poder escriure en el navegador web fem servir una variable de tipus PrintWriter.
			PrintWriter sortida = response.getWriter();
			sortida.println("hola");

			sortida.println("iNICIANT CONNEXIO AMB LA BD: " + servidorBDUrl);
						
			// Per a carregar el driver.
			Class.forName(driver);
			sortida.println("FET: Class.forName(driver)");
			
			// Per a connectar-nos a una BD farem servir la classe Connection.
			// Farem servir el mètode getConnection que té 3 paràmetres.
			// Fem una importació del paquet java.sql

			Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
			
			sortida.println("CONNEXIO AMB EXIT");
			
			// Tanquem la connexió.
			conexio.close();
			
			sortida.println("CONNEXIO TANCADA AMB EXIT");
			
		} catch (Exception e){
			PrintWriter sortida = response.getWriter();
			sortida.println("ERROR");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
