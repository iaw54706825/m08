package exercici_05_a_08.DAOs;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import exercici_05_a_08.entitats.Tripulant;

@Repository
public class TripulantDAOClasse implements TripulantDAOInterface{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	@Override
	public List<Tripulant> getTripulants() {
		
		Session laMevaSessio = sessionFactory.getCurrentSession();
		Query<Tripulant> queryLlistaTripulants = laMevaSessio.createQuery("from Tripulant", Tripulant.class);
		
		List<Tripulant> llistaTripulantDeLaBD = queryLlistaTripulants.getResultList();
		
		return llistaTripulantDeLaBD;
	}

	
}
