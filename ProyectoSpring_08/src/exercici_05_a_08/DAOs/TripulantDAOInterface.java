package exercici_05_a_08.DAOs;

import java.util.List;
import exercici_05_a_08.entitats.Tripulant;

public interface TripulantDAOInterface {

	public List<Tripulant> getTripulants();
}
