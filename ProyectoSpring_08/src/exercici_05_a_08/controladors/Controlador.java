package exercici_05_a_08.controladors;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import exercici_05_a_08.DAOs.*;
import exercici_05_a_08.entitats.Tripulant;

@Controller
@RequestMapping("/tripulant")
public class Controlador {
	
	@Autowired
	private TripulantDAOInterface tripulantDAO;
	
	@RequestMapping("/llistaTripulants")
	public String llistarTripulants(Model elModel) {
		
		List<Tripulant> llistaTripulantsDelDAO = tripulantDAO.getTripulants();
		elModel.addAttribute("llistaTripulants", llistaTripulantsDelDAO);
		
		return "llista_tripulants";
	}
}	
