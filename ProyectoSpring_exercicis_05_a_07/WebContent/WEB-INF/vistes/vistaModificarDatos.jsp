<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="exercici_05_a_07.Departament"%>
<%@page import="exercici_05_a_07.Personal"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	modificarDadesUsuari.jsp
	<br>
	<form:form action="mostrarDadesUsuari" modelAttribute="personal">
		<br>		
		DNI usuari: <form:input path="dni" />
		<br>
		Nom usuari: <form:input path="nom" />
		<br>
		Cognom usuari: <form:input path="cognom" />
		<form:errors path="cognom" style="color:red"></form:errors>		
		<br>
		Edat usuari: <form:input path="edat" />
		<form:errors path="edat" style="color:red"></form:errors>		
		<br>
		Email usuari: <form:input path="email" />
		<form:errors path="email" style="color:red"></form:errors>		
		<br>
		Contrasenya <form:input path="contrasenya" />&nbsp;&nbsp;	
		<form:errors path="contrasenya" style="color:red"></form:errors>
		<br>
		<br>
		Departament:&nbsp;&nbsp;&nbsp;&nbsp;
		<%
		List<Departament> llistaDepartaments = new ArrayList<Departament>();
		Personal personal = new Personal();
		personal = (Personal) request.getAttribute("personal");
		llistaDepartaments = (List<Departament>) request.getAttribute("listaDepartamentos");
		
		for (Departament dep : llistaDepartaments) {
			if (dep.getID() == personal.getDepartament()) {
		%>
		<form:radiobutton path="departament" value="<%=dep.getID()%>"
			label="<%=dep.getNom()%>" checked="checked" />
		<%
			} else {
		%>
		<form:radiobutton path="departament" value="<%=dep.getID()%>"
			label="<%=dep.getNom()%>" />
		<%
			}
		}
		%>


		<br>
		<input type="submit">
	</form:form>
</body>
</html>