package exercici_05_a_07;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	List<Personal> llistaPersonal;
	List<Departament> llistaDepartament;

	@RequestMapping("/")
	public String inici() {
		llistaDepartament = usAplicacion.inicialitzarDepartaments();
		llistaPersonal = usAplicacion.inicialitzarPersonal();
		// Comprobamos listas
		for (Personal per : llistaPersonal) {
			System.out.println(per);
		}		
		return "inici";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/modificarDadesUsuari")
	public String modificarDadesUsuari(HttpServletRequest request, Model model) {
		String mensajeError = "Error, no existe usuario";
		String dni = request.getParameter("dni");
		String pass = request.getParameter("pass");
		// Comprobamos usuario y contrase�a - (De momento solo el DNI)
		for (Personal p : llistaPersonal) {
			if (p.getDni().equals(dni)) {
//				if (!p.getContrasenya().equals(pass)) {
//					mensajeError = "Contraseña invalida";
//					model.addAttribute("mensaje",mensajeError);
//					return "login";
//				}
				model.addAttribute("personal", p);
				model.addAttribute("listaDepartamentos", this.llistaDepartament);			
				return "vistaModificarDatos";
			}
		}
		// Si el usuario no existe
		model.addAttribute("mensaje", mensajeError);
		return "login";
	}

	@RequestMapping("/mostrarDadesUsuari")
	public String mostrarDadesUsuari(@Valid @ModelAttribute("personal") Personal personal, BindingResult validacio, Model model) {
		//Es necesario volver a a�adir "llistaDepartament" (no se por que desaparece del modelo)
		model.addAttribute("listaDepartamentos", this.llistaDepartament);			
		if (validacio.hasErrors()) {		
			return "vistaModificarDatos";
		} else {
			return "mostrarDadesUsuari";
		}
	}

	@RequestMapping("/donarAltaUsuari")
	public String donarAltaUsuari() {
		return "donarAltaUsuari";
	}

	@RequestMapping("/confirmarAlta")
	public String confirmarAlta(HttpServletRequest request, Model model) {
		String dni = request.getParameter("dni");
		String nom = request.getParameter("nom");
		String pass = request.getParameter("pass");
		for (Personal p : llistaPersonal) {
			if (p.getDni().equals(dni)) {
				String error = "ERROR, ja existeix aquest DNI.";
				model.addAttribute("mensaje", error);
				return "donarAltaUsuari";
			}
		}
		// Añadimos Personal a la lista
		Personal p = new Personal(dni);
		p.setNom(nom);
		p.setContrasenya(pass);
		p.setDepartament(3);
		this.llistaPersonal.add(p);
		for (Personal per : llistaPersonal) {
			System.out.println(per);
		}
		return "login";
	}

}
