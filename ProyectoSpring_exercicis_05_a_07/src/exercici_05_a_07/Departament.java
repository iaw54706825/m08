package exercici_05_a_07;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component				// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
@Scope("prototype")
public class Departament {
	@Override
	public String toString() {
		return "Departament [ID=" + ID + ", nom=" + nom + "]";
	}


	private int ID;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	Personal capDeDepartament;
	ArrayList<Personal> llistaPersonal;
	
	
	/**
	 * @param iD
	 */
	public Departament(int iD) {
		super();
		ID = iD;
	}


	/**
	 * @param iD
	 * @param nom
	 * @param email
	 * @param organitzacio
	 */
	public Departament(int iD, String nom, String email, Organitzacio organitzacio) {
		super();
		ID = iD;
		this.nom = nom;
		this.email = email;
		this.organitzacio = organitzacio;
	}


	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}


	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}


	/**
	 * @param organitzacio the organitzacio to set
	 */
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}


	/**
	 * @return the capDeDepartament
	 */
	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}


	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}


	/**
	 * @return the llistaPersonal
	 */
	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}


	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}
	
	
	
	
}
