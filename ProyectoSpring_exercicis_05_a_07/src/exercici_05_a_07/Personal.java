package exercici_05_a_07;

import java.time.LocalDate;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component				// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
@Scope("prototype")
public class Personal {
	private String dni;
	private String nom;
	@NotBlank
	@Size(min = 2, message="Minimo 2 caracteres")
	@Size(max = 100, message="Maximo 100 caracteres")
	private String cognom;
	@Pattern(regexp=".+@[a-zA-Z]+\\.[a-zA-Z]+", message = "Formato email incorrecto")
	private String email;
	private String login;
	@NotBlank
	@NotNull
	@Size(min = 7, message="Contrase�a minima: 7 caracteres")
	private String contrasenya;
	private int departament;
	private String conneixements;
	private String ciutatNaixement;
	private String idiomes;
	@NotNull
	@Min(value = 18, message = "Edad minima: 18 a�os")
	private int edat;
	String telefon;
	LocalDate dataCreacio;
	/**
	 * @param dni
	 * @param nom
	 * @param cognom
	 * @param email
	 */
//	public Personal(String dni, String nom, String cognom, String email) {
//		super();
//		this.dni = dni;
//		this.nom = nom;
//		this.cognom = cognom;
//		this.email = email;
//	}
	public Personal() {		
	}
	/**
	 * Este constructor es el que usamos para crear los Beans(usAplicacion.java)
	 * @param dni
	 */
	public Personal(String dni) {
		super();
		this.dni = dni;
	}
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the contrasenya
	 */
	public String getContrasenya() {
		return contrasenya;
	}
	/**
	 * @param contrasenya the contrasenya to set
	 */
	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}
	/**
	 * @return the departament
	 */
	public int getDepartament() {
		return departament;
	}
	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(int departament) {
		this.departament = departament;
	}
	/**
	 * @return the conneixements
	 */
	public String getConneixements() {
		return conneixements;
	}
	/**
	 * @param conneixements the conneixements to set
	 */
	public void setConneixements(String conneixements) {
		this.conneixements = conneixements;
	}
	/**
	 * @return the ciutatNaixement
	 */
	public String getCiutatNaixement() {
		return ciutatNaixement;
	}
	/**
	 * @param ciutatNaixement the ciutatNaixement to set
	 */
	public void setCiutatNaixement(String ciutatNaixement) {
		this.ciutatNaixement = ciutatNaixement;
	}
	/**
	 * @return the idiomes
	 */
	public String getIdiomes() {
		return idiomes;
	}
	/**
	 * @param idiomes the idiomes to set
	 */
	public void setIdiomes(String idiomes) {
		this.idiomes = idiomes;
	}
	/**
	 * @return the edat
	 */
	public int getEdat() {
		return edat;
	}
	/**
	 * @param edat the edat to set
	 */
	public void setEdat(int edat) {
		this.edat = edat;
	}
	/**
	 * @return the telefon
	 */
	public String getTelefon() {
		return telefon;
	}
	/**
	 * @param telefon the telefon to set
	 */
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	/**
	 * @return the dataCreacio
	 */
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	@Override
	public String toString() {
		return "Personal [dni=" + dni + ", nom=" + nom + ", contrasenya=" + contrasenya + ", departament=" + departament
				+ "]";
	}

	
	
	
	
}
