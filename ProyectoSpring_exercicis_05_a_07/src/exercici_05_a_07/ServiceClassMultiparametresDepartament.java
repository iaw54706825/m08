package exercici_05_a_07;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component		// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
public class ServiceClassMultiparametresDepartament {
	private final BeanFactory factory;
	private List<Departament> llistaDepartamentsMultiparametres;
	
	/**
	 * Constructor
	 */
	@Autowired
	public ServiceClassMultiparametresDepartament(final BeanFactory f) {
		this.factory = f;
	}

	public void demoMethod(List<Integer> llistaIdsDepartament, List<String> llistaNomDepartament) {
		// Various versions of getBean() method return an instance of the specified bean.
		
		// Recorre tota la llista 'llistaIdsDepartament' a saco creant un flux amd les dades que conté (stream()) i 
		// per a cada element (param) aplica la funció que li passem dins del map() de manera que ens retorna
		// un flux amb tots els resultats (un flux amb els objectes de tipus Departament creats amb
		// 'factory.getBean(Departament.class, param)' i inicialitzats segons el
		// contructor Departament.public Departament(int id){}).
		//
		// Converteix el flux resultant en una llista gràcies al collect(Collectors.toList()). 
		// El collect() és el que canviarà el flux de dades en un altre cosa que serà una llista gràcies al paràmetre 'Collectors.toList()'.
		
		this.llistaDepartamentsMultiparametres = llistaIdsDepartament.stream().map(param -> factory.getBean(Departament.class, param))
				.collect(Collectors.toList());	
		
		int index = 0;
		for (Departament dept : this.llistaDepartamentsMultiparametres) {
			dept.setNom(llistaNomDepartament.get(index));			
			index++;
		}
	}

	public List<Departament> getLlistaDepartamentsMultiparametres() {
		// TODO Auto-generated method stub
		return llistaDepartamentsMultiparametres;
	}

}
