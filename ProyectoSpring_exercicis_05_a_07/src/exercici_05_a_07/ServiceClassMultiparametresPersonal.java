package exercici_05_a_07;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceClassMultiparametresPersonal {
	private final BeanFactory factory;
	private List<Personal> llistaPersonalMultiparametres;
	
	@Autowired
	public ServiceClassMultiparametresPersonal(final BeanFactory f) {
		this.factory = f;
	}
	
	public void demoMethod(List<String> llistaDniPersonal, List<String> llistaNomPersonal, List<LocalDate> llistaDataCreacio, List<Integer> departament) {
		// TODO Auto-generated method stub
		this.llistaPersonalMultiparametres = llistaDniPersonal.stream().map(param -> factory.getBean(Personal.class, param))
				.collect(Collectors.toList());	
		
		int index = 0;
		for (Personal per : this.llistaPersonalMultiparametres) {
			per.setNom(llistaNomPersonal.get(index));	
			per.setDataCreacio(llistaDataCreacio.get(index));
			per.setDepartament(departament.get(index));
			index++;
		}
		
	}

	public List<Personal> getLlistaPersonalMultiparametres() {
		// TODO Auto-generated method stub
		return llistaPersonalMultiparametres;
	}

}
