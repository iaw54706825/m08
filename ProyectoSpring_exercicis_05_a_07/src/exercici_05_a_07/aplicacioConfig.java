package exercici_05_a_07;

import java.time.LocalDate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("exercici_05_a_07")
public class aplicacioConfig {
	
}
