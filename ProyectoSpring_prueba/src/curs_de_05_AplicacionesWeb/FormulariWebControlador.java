package curs_de_05_AplicacionesWeb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;


@Controller
public class FormulariWebControlador {

	@RequestMapping("/mostrarFormulari")
	public String mostrarFormulari() {
		return "formulariNom";
	}
	
	@RequestMapping("/respostaDelFormulari_28")
	public String respostaDelFormulari() {
		return "formulariNomResposta";
	}
	
	@RequestMapping("/respostaDelFormulari_29")
	public String procesarFormulari(HttpServletRequest request, Model model) {
		String nomTripulant = request.getParameter("tripulantNom_29");
		String resposta = "Qui hi ha (video29)? " + nomTripulant + " es un tripulant de la CCCP Leonov";
		model.addAttribute("missatgePerRetornar_29", resposta);		
		return "formulariNomResposta";
	}
}

